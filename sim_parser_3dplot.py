import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pandas as pd


infile_path = "output.csv"

df = pd.read_csv(infile_path)
filtered_df = df[df['animal_id'].str.contains('Zebra:')]
selected_columns = filtered_df[["timestamp","latitude","longitude"]]


# Create a 3D plot
fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111, projection='3d')


# Generate random data

# Create the scatter plot
ax.scatter(selected_columns["latitude"], selected_columns["longitude"], selected_columns["timestamp"], c='blue', alpha=0.5)

# Set labels
ax.set_xlabel('X-axis')
ax.set_ylabel('Y-axis')
ax.set_zlabel('Z-axis')

# Set title
ax.set_title('3D Scatter Plot')

# Set grid
ax.grid(True)

# Show the plot
plt.show()