import csv
import json
import re
import pandas as pd
import matplotlib.pyplot as plt


infile_path = "output.csv"
#outfile_path = "output.csv"



df = pd.read_csv(infile_path)

filtered_df = df[df['animal_id'].str.contains('Zebra:')]
print(filtered_df)
filtered_df.to_csv('zebra_output.csv')