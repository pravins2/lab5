import csv
import json
import re
import pandas as pd
import matplotlib.pyplot as plt
import math
from math import radians, cos, sin, asin, sqrt

infile_path = "output.csv"
#outfile_path = "output.csv"

def haversine(lon1, lat1, lon2, lat2):
    """
    Calculate the great circle distance in kilometers between two points 
    on the earth (specified in decimal degrees)
    """
    # convert decimal degrees to radians 
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])

    # haversine formula 
    dlon = lon2 - lon1 
    dlat = lat2 - lat1 
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a)) 
    r = 6371 # Radius of earth in kilometers. Use 3956 for miles. Determines return value units.
    return c * r


df = pd.read_csv(infile_path)

filtered_df = df[df['animal_id'].str.contains('Zebra:')]


selected_columns = filtered_df[["animal_id","latitude","longitude"]]

grouped_df_mean = selected_columns.groupby('animal_id')[['latitude', 'longitude']].mean()

grouped_df_mean.to_csv("grouped_zebra.csv")

df_grp_zebra = pd.read_csv("grouped_zebra.csv")

print(df_grp_zebra)
out_obj = {}
out_list = []
for index, row in selected_columns.iterrows():
    #print(f"Index: {index}")
    #print(f"Row: {row}")
    #print(row['animal_id'])
    #if df_grp_zebra['animal_id'] == row['animal_id']:
    row_animal_id = row['animal_id']
    #print(row_animal_id)
    lat = (df_grp_zebra.loc[df_grp_zebra['animal_id'] == row_animal_id])['latitude']
    lng = (df_grp_zebra.loc[df_grp_zebra['animal_id'] == row_animal_id])['longitude']
    #print(haversine(lat,lng,row['latitude'],row['longitude']))
    out_obj['animal_id'] = row_animal_id
    #out_obj['timestamp'] = row['timestamp']
    out_obj['dist_from_mean'] = haversine(lat,lng,row['latitude'],row['longitude'])
    #if row_animal_id == 'Zebra:4adb29':
        #print(out_obj)
    #print(row)
    out_list.append(out_obj)
    out_obj = {}

#print(out_list)

animals = []
distances = []
for obj in out_list:
    animals.append(obj['animal_id'])
    distances.append(obj['dist_from_mean'])

# Create a bar graph
plt.figure(figsize=(8, 6))
plt.bar(animals, distances, color='skyblue')
plt.xlabel('Zebra Id')
plt.ylabel('Distance travelled from mean in meters')
plt.title('Zebra distance travelled by meters')
plt.grid(True)
plt.show()