import csv
import json
import re
import pandas as pd
import matplotlib.pyplot as plt


infile_path = "output.csv"
#outfile_path = "output.csv"



df = pd.read_csv(infile_path)

filtered_df = df[df['animal_id'].str.contains('Zebra:')]


selected_columns = filtered_df[["animal_id","oxygen_saturation","heart_rate"]]

grouped_df = selected_columns.groupby('animal_id')[['oxygen_saturation', 'heart_rate']].mean()

print(grouped_df)
# Plot the mean of the `sales` and `price` columns for each product in a line chart
plt.plot(grouped_df.index, grouped_df['oxygen_saturation'], label='oxygen_saturation')
plt.plot(grouped_df.index, grouped_df['heart_rate'], label='heart_rate')

plt.xlabel('Animal')
plt.ylabel('oxygen_saturation and heart rate')
plt.title('Oxygen saturation and heart rate reading for Zebra herd')
plt.legend()
plt.grid(True)
plt.show()

