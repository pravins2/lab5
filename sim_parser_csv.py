import csv
import json
import re



# method for parsing only the GPS data at each timestamp for each distinct animal using regex
def parse_gps_data(file_path : str) -> dict:
    """
    Parses GPS data at each timestamp for each distinct animal from a READABLE simulation log file and returns it as a dictionary
    
    This function reads a file from the World_win/StandalongWindows64/SavannaLogs/Simulation/simulation_2023_xx_xx_xx_xx_READABLE.txt format
    and extracts the timestamp and GPS coordinates recorded for each distinct animal; formatting the results into a dictionary which can then
    be processed into a clean .json file at the end of this script. 

    Args:
        file_path (str): The path to the *READABLE* .txt file containing the GPS data for your animals.

    Returns:
        dict: A dictionary containing all parsed GPS, timestamp, and animal data which can be saved as a clean .json and/or loaded
              into a pandas DataFrame for futher analysis.
    """
    
    # setup attributes for storing data and keeping track of the current (distinct) animal
    data = {}
    current_animal = None
    current_timestamp_main = None

    # define some regular expression patterns for grabbing the animal id, the timestamp, and the gps coordinates
    #=============== Zebra:4adb29 LOG ===============
    animal_pattern = re.compile(r'^=============== (.*?):(.*?) LOG ===============$')
    timestamp_pattern = re.compile(r'-- Timestamp: ([\d.]+)')
    location_pattern = re.compile(r'"location": \[([-?\d.]+),([-?\d.]+)\]')
    oxygen_saturation_pattern = re.compile(r'"oxygen_saturation": ([\d.]+), "heart_rate": ([\d.]+)')
    temperature_pattern = re.compile(r'"temperature": ([\d.]+)')
    humidity_pattern = re.compile(r'"humidity": ([\d.]+), "temperature": ([\d.]+)')

    # open the *READABLE* simulation log file
    with open(file_path, 'r') as file:
        data = []
        record = {}
        for line in file:
            # check for animal type and id and create new entries if (new) distinct animal
            animal_match = animal_pattern.match(line.strip())
            if animal_match:
                animal_type = animal_match.group(1)
                animal_id = animal_match.group(2)
                current_animal = f"{animal_type}:{animal_id}"
                #if current_animal is not None and current_timestamp_main is not None:
                #    data.append(record)
                #    record = {}    
                # set up attributes reflecting the timestamps and gps coordinates for the distinct animal represented as lists
                #data[current_animal] = {"timestamp": [], "location": [],"oxygen_saturation":[],"heart_rate":[],"temperature":[],"humidity":[]}
                continue

            # check for timestamp and location
            if current_animal:
                timestamp_match = timestamp_pattern.match(line.strip())
                location_match = location_pattern.match(line.strip())
                oxygen_saturation_match = oxygen_saturation_pattern.match(line.strip())
                temperature_pattern_match = temperature_pattern.match(line.strip())
                humidity_match = humidity_pattern.match(line.strip())

                record["animal_id"] = current_animal
                # if there is a new timestamp, we add it to the list of timestamps for the distinct animal
                if timestamp_match:
                    current_timestamp = timestamp_match.group(1)
                    #data[current_animal]["timestamp"].append(current_timestamp)
                    #record["timestamp"] = current_timestamp
                    if current_timestamp is None or current_timestamp != current_timestamp_main:
                        current_timestamp_main = current_timestamp
                        record["timestamp"] = current_timestamp_main
                # if there is a location match, we perform the same operation with the gps coordinates
                elif location_match:
                    #location = (location_match.group(1), location_match.group(2))
                    #data[current_animal]["location"].append(location)
                    record["latitude"] = location_match.group(1)
                    record["longitude"] = location_match.group(2)

                # if there is a location match, we perform the same operation with the gps coordinates
                elif oxygen_saturation_match:
                    oxygen_saturation = oxygen_saturation_match.group(1)
                    heart_rate = oxygen_saturation_match.group(2)
                    #data[current_animal]["oxygen_saturation"].append(oxygen_saturation)
                    #data[current_animal]["heart_rate"].append(heart_rate)
                    record["oxygen_saturation"] = oxygen_saturation
                    record["heart_rate"] = heart_rate

                elif temperature_pattern_match:
                    temperature = temperature_pattern_match.group(1)
                    #data[current_animal]["temperature"].append(temperature)
                    record["temperature"] = temperature

                elif humidity_match:
                    humidity = humidity_match.group(1)
                    #data[current_animal]["humidity"].append(humidity)
                    record["humidity"] = humidity
                    #print(record)
                    data.append(record)
                    record = {}

            #data.append(record)

    return data




infile_path = "simulation_2023_11_22_17_57_45_READABLE.txt"
outfile_path = "output.csv"

# parse the READABLE simulation log
parsed_data = parse_gps_data(infile_path)
print(parsed_data)

# Create a CSV writer object and specify the column names
with open(outfile_path, 'w') as csvfile:
    writer = csv.DictWriter(csvfile, fieldnames=['animal_id', 'timestamp', 'latitude','longitude','oxygen_saturation','heart_rate','temperature','humidity'])

    # Write the header row
    writer.writeheader()

    # Write the data rows
    for row in parsed_data:
        writer.writerow(row)


