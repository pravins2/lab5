import pandas as pd
import matplotlib.pyplot as plt
import math
import numpy as np 
from math import radians, cos, sin, asin, sqrt

infile_path = "output.csv"
#outfile_path = "output.csv"

def haversine(lon1, lat1, lon2, lat2):
    """
    Calculate the great circle distance in kilometers between two points 
    on the earth (specified in decimal degrees)
    """
    # convert decimal degrees to radians 
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])

    # haversine formula 
    dlon = lon2 - lon1 
    dlat = lat2 - lat1 
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a)) 
    r = 6371 # Radius of earth in kilometers. Use 3956 for miles. Determines return value units.
    return c * r


df = pd.read_csv(infile_path)
filtered_df = df[df['animal_id'].str.contains('Zebra:')]
selected_columns = filtered_df[["animal_id","timestamp","latitude","longitude"]]


prev_lat = None
prev_lng = None
result = []
for index, row in selected_columns.iterrows():
    record = {}
    row_animal_id = row['animal_id']
    #print(row_animal_id)
    lat = row['latitude']
    lng = row['longitude']
    dist = None
    if prev_lat is not None:
        dist = haversine(prev_lng,prev_lat,lng,lat)
    else:
        dist = 0

    prev_lat = lat
    prev_lng = lng
    record['animal_id'] = row_animal_id
    record['timestamp'] = row['timestamp']
    record['distance'] = dist
    result.append(record)

dist_list = []
for obj in result:
    #print(obj['distance'])
    dist_list.append(obj['distance'])
print(dist_list)
dist_list.sort()

# Calculate the cumulative sum
cdf = np.cumsum(dist_list) / len(dist_list)

# Plot the CDF
plt.plot(dist_list, cdf)
plt.xlabel('CDF')
plt.ylabel('CDF of Movement speed of Zebra')
plt.title('CDF')
plt.grid(True)
plt.show()
print(result)